module.exports = {
    bytesToInt: function (bytes) {
        let result = 0

        result = result | (0xFF000000 & parseInt(bytes[3]) << 24)
        result = result | (0x00FF0000 & parseInt(bytes[2]) << 16)
        result = result | (0x0000FF00 & parseInt(bytes[1]) << 8)
        result = result | (0x000000FF & parseInt(bytes[0]) << 0)

        return result
    },

    padLeft: function (str, len, rep) {

        if (str.length < len) {
            var dif = len - str.length;

            var rec = "";
            while (dif > 0) {
                rec += rep + '';
                dif--;
            }

            str = rec + '' + str
        }

        return str
    },

    strToBin: function (text) {
        return text.split('').map(letter => {
            // console.log(letter, letter.charCodeAt(0), letter.charCodeAt(0).toString(2), this.padLeft(letter.charCodeAt(0).toString(2), 8, 0))

            return this.padLeft(letter.charCodeAt(0).toString(2), 8, 0);
        }).reduce((previous, current) => {
            return previous + current
        }, '')
    },

    binToStr: function (sequence) {
        let text = ''

        for (let i = 0; i < sequence.length; i += 8) {
            text += String.fromCharCode(parseInt(sequence.substring(i, i + 8), 2))
        }

        return text
    }
}