const util = require('./util.js')

const fs = require('fs')
const { Transform } = require('stream')

const messageBreak = '<<<'
const outputFile = 'outputFile.bmp'
const readStream = fs.createReadStream(outputFile)

const showMessage = () => new Transform({
    transform: function (chunk, encoding, callback) {

        const data = chunk

        if (!this.offset) {
            this.offset = 0
        }

        if (this.offset === 0) {
            const id = (String.fromCharCode(data[0], data[1]))

            if (id === 'BM') {
                console.log('Image is a Bitmap')
            }

            let messageBin = ''
            const messageBreakBin = util.strToBin(messageBreak)

            const dataOffset = util.bytesToInt([data[10], data[11], data[12], data[13]])
            this.dataOffset = dataOffset

            for (let i = dataOffset; i < chunk.length; i++) {
                messageBin += chunk[i] % 2

                if (messageBreakBin.length % 8 === 0) {
                    if (messageBin.indexOf(messageBreakBin) >= 0) {
                        messageBin = messageBin.substring(0, messageBin.indexOf(messageBreakBin))
                        break;
                    }
                }
            }

            console.log('Message: ', util.binToStr(messageBin))
        }

        this.offset += chunk.length

        callback()
    }
})

readStream.pipe(showMessage())