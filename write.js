const util = require('./util.js')

const fs = require('fs')
const { Transform } = require('stream')

const inputFile = 'inputFile.bmp'
const readStream = fs.createReadStream(inputFile)

const outputFile = 'outputFile.bmp'
const writeStream = fs.createWriteStream(outputFile)

const messageBreak = '<<<'

const hideMessage = message => new Transform({
    transform: function (chunk, encoding, callback) {

        const data = chunk

        if (!this.offset) {
            this.offset = 0
        }

        if (this.offset === 0) {
            const id = (String.fromCharCode(data[0], data[1]))

            if (id === 'BM') {
                console.log('Image is a Bitmap')
            }

            const tamanho = util.bytesToInt([data[2], data[3], data[4], data[5]])
            console.log('Tamanho: ', tamanho)

            const dataOffset = util.bytesToInt([data[10], data[11], data[12], data[13]])
            this.dataOffset = dataOffset
            console.log('dataOffset: ', dataOffset)

            const disponivel = tamanho - dataOffset
            console.log('Disponivel: ', disponivel)

            const messageBin = util.strToBin(message + messageBreak)
            console.log('messageBin: ', messageBin)

            if (messageBin.length < disponivel) {
                console.log('It is possible')
            } else {
                console.log('It isnt possible')
            }

            console.log('data: ', data[dataOffset], data[dataOffset + 1], data[dataOffset + 2], data[dataOffset + 3], data[dataOffset + 4], data[dataOffset + 5], data[dataOffset + 6])

            messageBin.split('').forEach(bit => {
                chunk[this.dataOffset] -= chunk[this.dataOffset] % 2
                chunk[this.dataOffset] += parseInt(bit)
                this.dataOffset++
            })
        }

        this.push(chunk)
        this.offset += chunk.length

        callback()
    }
})

readStream.pipe(hideMessage('Anderson H Botega')).pipe(writeStream)