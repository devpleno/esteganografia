const util = require('./util.js')

const fs = require('fs')
const file = 'inputFile.bmp'
const readStream = fs.createReadStream(file)

let size = 0
readStream.on('data', (data) => {

    if (size === 0) {
        const id = (String.fromCharCode(data[0], data[1]))

        if (id === 'BM') {
            console.log('Image is a Bitmap')
        }

        const tamanho = util.bytesToInt([data[2], data[3], data[4], data[5]])
        console.log('Bitmap size: ', tamanho)

        const disponivel = tamanho - util.bytesToInt([data[10], data[11], data[12], data[13]])
        console.log('Bitmap avaliable: ', disponivel)

        const message = "Anderson Botega"

        const messageBin = util.strToBin(message)
        console.log(messageBin)
        console.log('Tamanho necessario: ', messageBin.length)

        if (messageBin.length < disponivel) {
            console.log('É possivel')
        } else {
            console.log('Não é possivel')
        }
    }

    size += data.length
})

readStream.on('end', () => {
    console.log(size / 1024 / 1024)
})